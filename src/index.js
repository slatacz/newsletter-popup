import React from 'react';
import ReactDOM from 'react-dom';
import Newsletter from './Newsletter';

const props = {
  header: 'zapisz się do newslettera',
  description:'Zapisz się do do newslettera, żeby otrzymać 10% zniżki na pierwsze zakupy!',
  placeholder: 'Wpisz swoj adres e-mail...',
  buttonCaption:'Zapisz się '
}

ReactDOM.render(
  <React.StrictMode>
      <Newsletter {...props}/>
  </React.StrictMode>,
  document.getElementById('root')
);
