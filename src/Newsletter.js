import styled,{ keyframes} from 'styled-components';
import useWindowDimensions from './Window'
import { useState,useEffect, useRef } from 'react';



const Newsletter = (props) => {
  const { height, width } = useWindowDimensions();
  const colors = ['#ff598a','#de56e8','#b36bff','#5b56e8','#5e9fff'];
  const Container = styled.section`
    background-color: #070222;
    width: ${width}px;
    height: ${height}px;
    position: absolute;
    left: 0;
    top:0;
  `;
  const Box = styled.div`
    background-color: #2b283d;
    position: relative;
    top: 20%;
    left: 50%;
    transform: translateX(-50%);
    width: 40%;
    padding: 20px;
  `;
  const Header = styled.h1`
    padding: 30px 0 20px 0;
    text-align: center;
    text-transform: uppercase;
    font-weight: 700;
    color:#fff;
    font-size: 2em;
    text-shadow: 0 3px 2px #000;
    margin:0;
    z-index:1;
  `;
  const Description = styled.p`
    text-align: center;
    font-size: 1.2em;
    color:#fff;
    text-shadow: 0 3px 2px #000;
    padding-bottom: 20px;
    max-width: 80%;
    margin: auto;
    z-index:1;
  `;
  const StyledInput = styled.input`
    position: relative;
    height: 3em;
    line-height: 3em;
    width: 80%;
    left: 10%;
    align-content: center;
    margin: 20px 0 50px 0;
    font-size: 1.4em;
    border: 1px solid black;
    color: #fff;
    background: #070222;
    text-align: center;
  }
  &:focus {
    outline-offset: 0.3em;
    outline: 2px solid #5e9fff;
  }
  `;
  const fadeIn = keyframes`
  from {
    opacity: 0;
    transform: translate(-50%, 50%) scale(0);
  }

  to {
    opacity: 1;
    transform: translate(-50%, 50%) scale(1);
  }
`;

const fadeOut = keyframes`
  from {
    opacity: 1;
    transform: translate(-50%, 50%) scale(1);
  }

  to {
    opacity: 0;
    transform: translate(-50%, 50%) scale(0);
  }
`;
  const ActiveButton = styled.button`
    position: absolute;
    left: 50%;
    transform: translate(-50%, 50%) rotate(-5deg);
    height: 20%;
    bottom: 0;
    overflow: hidden;
    padding: 0;
    margin: 0;
    background: #fff;
    border: 0;
    text-transform: uppercase;
    visibility: ${props => props.visibilit ? 'visible' : 'hidden'};
    animation: ${props => props.visibilit ? fadeIn : fadeOut} 0.3s linear;
    transition: all 0.3s linear;
    font-size: 1.5em;
    padding: 0.5em 2em;
    z-index: 1;
    color: #070222;
    font-weight: bold;
    border-bottom: 3px solid ${colors[0]};
    cursor: pointer;
    &:focus{
      outline: 2px solid #fff;
      outline-offset: 4px;
    }
    &:focus, &:hover{
      border-bottom-color: ${colors[4]};
      transform: translate(-50%, 50%) rotate(0deg) scale(1.2);
    }
  `;
  const SpectrumContainer = styled.div`
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    width: 100%;
    z-index:-1;
    display: flex;
    align-items: flex-end;
    pointer-events: none;
  `;
  const jitter = keyframes`
    0% {
      transform: scaleY(1);
    }
    100% {
      transform: scaleY(0.9);
    }
  `;
  const Bar = styled.div`
    height: ${props => props.height};
    transition: all 0.3s linear ;
    width: 20%;
    animation: ${jitter} 350ms ease-out infinite alternate;
    transform-origin: bottom;
    transition: all 1s ease;
    animation-delay: ${props =>props.delayBar}ms;
    background: ${props =>props.color};
  `;
  const countEmailParts = (email) => {
    if (/@.+\..{2,}$/.test(email)) {
      return 5
    } else if (/@.+\..?$/.test(email)) {
      return 4
    } else if (/@.+$/.test(email)) {
      return 3
    } else if (/@/.test(email)) {
      return 2
    } else if (/.+/.test(email)) {
      return 1
    } else {
      return 0
    }
  }

  const [email, setEmail] = useState('');
  const emailInput = useRef(null);
  const emailPartsCount = countEmailParts(email);
  useEffect(() => {
    emailInput.current.focus();
  }, [email]);
  return (
    <Container >
      <Box >
        <SpectrumContainer aria-hidden>
          {colors.map((color,i)=>(
            <Bar color={color} key={i} height={ i + 1 <= emailPartsCount ? '100%': '5%'} delayBar={i*50}></Bar>
          ))}
        </SpectrumContainer>
        <header>
          <Header>
            {props.header}
          </Header>
        </header>
        <Description>
          {props.description}
        </Description>
        <StyledInput 
          placeholder={props.placeholder}
          type="email"
          ref={emailInput}
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        <ActiveButton visibilit={emailPartsCount === 5 ? true: false}>
          {props.buttonCaption}
        </ActiveButton>
        
      </Box>
    </Container>
  );
}

export default Newsletter;
